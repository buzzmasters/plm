﻿CREATE TABLE public.users
(
    id serial PRIMARY KEY,
    username varchar(30)
    password varchar(30)
    roleId integer 

	constraint fk_users_roles
		foreign key (roleId) 
		REFERENCES roles (id)
)
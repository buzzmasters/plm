﻿namespace Uni.Plm.Auth.Model
{
    public class Role
    {
        public int? Id { get; set; }
        
        public string Name { get; set; }
    }
}

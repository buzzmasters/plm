﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Uni.Plm.Auth.Model
{
    public class User
    {
        public int? Id { get; set; }
        
        public string Username { get; set; }
        
        public string Password { get; set; }

        public string Email { get; set; }

        [ForeignKey("Role")]
        public int? RoleId { get; set; }
        
        public virtual Role Role { get; set; }
    }
}

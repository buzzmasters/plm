﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Protocols;
using NUnit.Framework;
using Uni.Plm.Auth.Model;

namespace Uni.Plm.Auth.Service.Test
{
    [TestFixture]
    public class AuthContextTests
    {
        public IConfigurationRoot Configuration { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            Configuration = builder.Build();
        }

        [Test]
        public void Can_Add_To_Db()
        {
            using (var ctx = new AuthContext(Configuration.GetConnectionString("AuthDatabase")))
            {
                var s = ctx.Roles.Add(new Role()
                {
                    Name = "Admin"
                });

                ctx.SaveChanges();
            }

            Assert.True(true);
        }
    }
}

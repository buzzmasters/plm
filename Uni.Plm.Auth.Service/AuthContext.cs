﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BCrypt;
using Microsoft.EntityFrameworkCore;
using Uni.Plm.Auth.Model;
using Uni.Plm.Auth.Service.ViewModels;

namespace Uni.Plm.Auth.Service
{
    public class AuthContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        private readonly string _connectionString;

        public AuthContext() : this(ServiceConfiguration.AuthDatabaseConnectionString)
        {
        }

        public AuthContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public User Find(string username, string password)
        {
            User person = Users.FirstOrDefault(x => x.Username.Equals(username, StringComparison.OrdinalIgnoreCase));

            if (person == null || !BCryptHelper.CheckPassword(password, person.Password)) return null;

            return person;
        }

        public User Register(UserViewModel user)
        {
            user.Username = user.Username.ToLower();

            if (UserExists(user.Username))
            {
                return null;
            }

            var newUser = Users.Add(new User()
            {
                Username = user.Username,
                Password = BCryptHelper.HashPassword(user.Password, BCryptHelper.GenerateSalt(12)),
                Email = user.Email
            });

            SaveChanges();

            return newUser.Entity;
        }

        private bool UserExists(string username)
        {
            return Users.Any(x => x.Username.Equals(username, StringComparison.OrdinalIgnoreCase));
        }
    }
}

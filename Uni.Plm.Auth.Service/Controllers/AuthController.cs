﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BCrypt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Uni.Plm.Auth.Model;
using Uni.Plm.Auth.Service.ViewModels;

namespace Uni.Plm.Auth.Service.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        [Authorize]
        [HttpGet]
        public int K()
        {
            return 4;
        }

        [HttpPost("login")]
        public async Task Login([FromBody]UserViewModel user)
        {
            if (string.IsNullOrEmpty(user.Username))
            {
                throw new ArgumentNullException("Username");
            }

            if (string.IsNullOrEmpty(user.Password))
            {
                throw new ArgumentNullException("Password");
            }

            var identity = GetIdentity(user.Username, user.Password);
            if (identity == null)
            {
                Response.StatusCode = 400;
                await Response.WriteAsync("Invalid username or password.");
                return;
            }

            var now = DateTime.UtcNow;
            
            var jwt = new JwtSecurityToken(
                //issuer: AuthOptions.ISSUER,
                //audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromHours(3)),
                signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
                );

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };
            
            Response.ContentType = "application/json";
            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes("mysupersecret_secretkey!123"));
        }

        [HttpPost("register")]
        public async Task Register([FromBody]UserViewModel user)
        {
            if (string.IsNullOrEmpty(user.Username))
            {
                throw new ArgumentNullException("Username");
            }

            if (string.IsNullOrEmpty(user.Password))
            {
                throw new ArgumentNullException("Password");
            }

            if (string.IsNullOrEmpty(user.Email))
            {
                throw new ArgumentNullException("Email");
            }

            using (var ctx = new AuthContext())
            {
                User newUser = ctx.Register(user);

                if (newUser == null)
                {
                    Response.StatusCode = 400;
                    await Response.WriteAsync("User with such username already exists.");
                    return;
                }

                await Response.WriteAsync(JsonConvert.SerializeObject(
                    new UserViewModel {Id = newUser.Id, Username = newUser.Username, Role = newUser.Role?.Name},
                    new JsonSerializerSettings {Formatting = Formatting.Indented, NullValueHandling = NullValueHandling.Ignore}));
            }
        }

        private ClaimsIdentity GetIdentity(string username, string password)
        {
            using (var ctx = new AuthContext())
            {
                User person = ctx.Find(username, password);
                if (person != null)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, person.Username),
                        new Claim("UserId", person.Id.ToString())
                    };

                    claims.Add(person.RoleId.HasValue
                        ? new Claim(ClaimsIdentity.DefaultRoleClaimType, person.RoleId.ToString())
                        : new Claim(ClaimsIdentity.DefaultRoleClaimType, "Loser"));

                    ClaimsIdentity claimsIdentity =
                        new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                            ClaimsIdentity.DefaultRoleClaimType);
                    return claimsIdentity;
                }
                
                return null;
            }
        }
    }
}

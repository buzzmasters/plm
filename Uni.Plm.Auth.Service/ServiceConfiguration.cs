﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uni.Plm.Auth.Service
{
    public static class ServiceConfiguration
    {
        public static string AuthDatabaseConnectionString { get; set; }
    }
}

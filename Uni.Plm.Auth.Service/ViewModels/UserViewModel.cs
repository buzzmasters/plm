﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Uni.Plm.Auth.Service.ViewModels
{
    public class UserViewModel
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        public string Email { get; set; }

        [JsonProperty("roleId")]
        public int? RoleId { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }
    }
}

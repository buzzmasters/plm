﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Uni.Plm.Common.Configs.Application
{
    public class ApplicationConfig
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("pages")]
        public PagesConfig Pages { get; set; }
    }
}

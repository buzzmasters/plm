﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Uni.Plm.Common.Configs.Application
{
    public class PagesConfig
    {
        [JsonProperty("master")]
        public string Master { get; set; }
        [JsonProperty("partials")]
        public List<string> Partials { get; set; }
    }
}

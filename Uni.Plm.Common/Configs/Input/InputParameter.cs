﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Uni.Plm.Common.Converters;

namespace Uni.Plm.Common.Configs.Input
{
    public class InputParameter
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("inputType")]
        [JsonConverter(typeof(StringInputTypeConverter))]
        public InputTypeEnum InputType { get; set; }
        [JsonProperty("dataType")]
        [JsonConverter(typeof(StringDataTypeConverter))]
        public DataTypeEnum DataType { get; set; }
        [JsonProperty("isRequired")]
        public bool IsRequired { get; set; }
        [JsonProperty("inGlobal")]
        public bool InGlobal { get; set; }
        [JsonProperty("defaultValue")]
        public string DefaultValue { get; set; }
    }
}

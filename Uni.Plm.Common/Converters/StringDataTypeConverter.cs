﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Uni.Plm.Common.Configs.Input;

namespace Uni.Plm.Common.Converters
{
    public class StringDataTypeConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            DataTypeEnum inputType = (DataTypeEnum)value;

            switch (inputType)
            {
                case DataTypeEnum.Number:
                    writer.WriteValue("number");
                    break;
                case DataTypeEnum.String:
                    writer.WriteValue("string");
                    break;
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = (string)reader.Value;

            switch (value)
            {
                case "number":
                    return DataTypeEnum.Number;
                case "string":
                    return DataTypeEnum.String;
                default:
                    throw new Exception($"Can't parse Data Type value '{value}'.");
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }
    }
}

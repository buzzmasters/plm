﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Uni.Plm.Common.Configs.Input;

namespace Uni.Plm.Common.Converters
{
    public class StringInputTypeConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            InputTypeEnum inputType = (InputTypeEnum)value;

            switch (inputType)
            {
                case InputTypeEnum.Textbox:
                    writer.WriteValue("textbox");
                    break;
                case InputTypeEnum.Checkbox:
                    writer.WriteValue("checkbox");
                    break;
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = (string)reader.Value;

            switch (value)
            {
                case "textbox":
                    return InputTypeEnum.Textbox;
                case "checkbox":
                    return InputTypeEnum.Checkbox;
                default:
                    throw new Exception($"Can't parse Input Type value '{value}'.");
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uni.Plm.Common.Entities
{
    public class ProjectData
    {
        public int Id { get; set; }
        public Guid ProjectId { get; set; }
        public Guid? ModuleId { get; set; }
        public string ParameterId { get; set; }
        public string ParameterValue { get; set; }
        public string Type { get; set; }
    }
}

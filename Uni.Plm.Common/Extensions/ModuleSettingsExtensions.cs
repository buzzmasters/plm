﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uni.Plm.Common.Extensions
{
    public static class ModuleSettingsExtensions
    {
        public static string GetParameterValue(this ModuleSettings moduleSettings, string key)
        {
            if (!moduleSettings.GlobalParameters.TryGetValue(key, out string result))
            {
                moduleSettings.Parameters.TryGetValue(key, out result);
            }

            return result;
        }

        public static bool? GetBoolParameterValue(this ModuleSettings moduleSettings, string key)
        {
            string value = GetParameterValue(moduleSettings, key);
            
            if (!string.IsNullOrEmpty(value))
            {
                if (!bool.TryParse(value, out bool result))
                {
                    throw new Exception($"Unknown bool value '{value}' for parameter key '{key}'");
                }

                return result;
            }

            return null;
        }
    }
}

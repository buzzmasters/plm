﻿namespace Uni.Plm.Common
{
    public interface IModule
    {
        IModule Initialize(ModuleSettings parameters);
    }
}

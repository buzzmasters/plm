﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uni.Plm.Common
{
    public interface IModuleSettings
    {
        Dictionary<string, string> GlobalParameters { get; }
        Dictionary<string, string> Parameters { get; }
    }
}

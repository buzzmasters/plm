﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uni.Plm.Common
{
    public class ModuleBase : IModule
    {
        private ModuleSettings _moduleSettings;

        public ModuleSettings Settings => _moduleSettings;

        public virtual IModule Initialize(ModuleSettings parameters)
        {
            _moduleSettings = parameters;
            return this;
        }
    }
}

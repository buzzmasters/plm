﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uni.Plm.Common.ModuleManager
{
    public interface IModuleManagerService
    {
        string GetParameterValue(string key, Guid projectId, bool isRequired = true);
        void SetParameterValue(string key, Guid projectId, string value);
    }
}

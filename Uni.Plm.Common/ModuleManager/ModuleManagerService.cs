﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Uni.Plm.Common.Entities;

namespace Uni.Plm.Common.ModuleManager
{
    public class ModuleManagerService : IModuleManagerService
    {
        private readonly PlmCommonContext _context;

        public ModuleManagerService(PlmCommonContext context)
        {
            _context = context;
        }

        public string GetParameterValue(string key, Guid projectId, bool isRequired = true)
        {
            string value = _context.ProjectData.FirstOrDefault(d => d.ProjectId.Equals(projectId) && d.ParameterId.Equals(key))?.ParameterValue;
            if (isRequired && string.IsNullOrEmpty(value))
            {
                throw new Exception($"Missing required parameter with Id: '{key}'");
            }

            return value;
        }

        public void SetParameterValue(string key, Guid projectId, string value)
        {
            var data = _context.ProjectData.FirstOrDefault(d => d.ProjectId.Equals(projectId) && d.ParameterId.Equals(key));
            if (data != null)
            {
                data.ParameterValue = value;
                _context.ProjectData.Update(data);
            }
            else
            {
                _context.ProjectData.Add(new ProjectData()
                {
                    ProjectId = projectId,
                    ParameterId = key,
                    ParameterValue = value
                });
            }
            _context.SaveChanges();
        }
    }
}

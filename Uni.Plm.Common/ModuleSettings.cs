﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uni.Plm.Common
{
    public class ModuleSettings
    {
        public Dictionary<string, string> GlobalParameters { get; }
        public Dictionary<string, string> Parameters { get; }
    }
}

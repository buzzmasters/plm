﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Uni.Plm.Common.Entities;

namespace Uni.Plm.Common
{
    public class PlmCommonContext : DbContext
    {
        public DbSet<ProjectData> ProjectData { get; set; }

        public PlmCommonContext(DbContextOptions<PlmCommonContext> options)
            : base(options)
        { }

        public PlmCommonContext(string connectionString) : this(GetOptions(connectionString))
        { }

        private static DbContextOptions<PlmCommonContext> GetOptions(string connectionString)
        {
            return new DbContextOptionsBuilder<PlmCommonContext>().UseSqlServer(connectionString).Options;
        }
    }
}

﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Uni.Plm.Common;
using Uni.Plm.Common.ModuleManager;

namespace Uni.Plm.Test0.Module.Areas.Uni.Plm.Test0.Module.Controllers
{
    [ModuleComponent]
    [Area("Uni.Plm.Test0.Module")]
    public class ModuleController : Controller
    {
        private readonly IModuleManagerService _moduleManager;

        public ModuleController(PlmCommonContext context)
        {
            _moduleManager = new ModuleManagerService(context);
        }

        public ActionResult Index()
        {
            return View(0);
        }

        [HttpPost]
        [Route("Uni.Plm.Test0.Module/module/set")]
        public void SetParameterValue([FromBody] Parameter parameter)
        {
            _moduleManager.SetParameterValue(parameter.Key, parameter.ProjectId, parameter.Value);
        }

        public class Parameter
        {
            public string Key { get; set; }
            public string Value { get; set; }
            public Guid ProjectId { get; set; }
        }
    }
}

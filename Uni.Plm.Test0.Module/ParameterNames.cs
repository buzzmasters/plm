﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uni.Plm.Test0.Module
{
    public class ParameterNames
    {
        public static readonly string Organization = "Project.Organization";
        public static readonly string LeadEngineerName = "Project.LeadEngineerName";
    }
}

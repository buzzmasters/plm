﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Uni.Plm.Common;
using Uni.Plm.Common.ModuleManager;

namespace Uni.Plm.Test1.Module.Areas.Uni.Plm.Test1.Module.Controllers
{
    [ModuleComponent]
    [Area("Uni.Plm.Test1.Module")]
    public class ModuleController : Controller
    {
        private readonly IModuleManagerService _moduleManager;

        public ModuleController(PlmCommonContext context)
        {
            _moduleManager = new ModuleManagerService(context);
        }

        public ActionResult Index(Guid projectId)
        {
            var model = new List<Parameter>();
            var organization = _moduleManager.GetParameterValue(ParameterNames.Organization, projectId);
            var lead = _moduleManager.GetParameterValue(ParameterNames.LeadEngineerName, projectId);
            model.Add(new Parameter()
            {
                Value = organization,
                Key = ParameterNames.Organization,
                ProjectId = projectId
            });
            model.Add(new Parameter()
            {
                Value = lead,
                Key = ParameterNames.LeadEngineerName,
                ProjectId = projectId
            });
            return View(model);
        }

        public class Parameter
        {
            public string Key { get; set; }
            public string Value { get; set; }
            public Guid ProjectId { get; set; }
        }
    }
}

import {Component, Input, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {Project} from '../Shared/_models/project.model';
import {ProjectsService} from '../Shared/_services/projects.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['home.component.scss']
})
export class HomeComponent implements OnInit {
  projects: Project[] = [];

  constructor(private toastr: ToastrService, private router: Router, private projectService: ProjectsService) { }

  showUnauthorized() {
    setTimeout(() => this.toastr.error('Unauthorized. Please log into the system.'));
  }

  ngOnInit(): void {

    const data = this.router.getNavigatedData();
    if (data && data.is401) {
      this.showUnauthorized();
    } else {
      this.updateProjectsList();
    }

  }

  updateProjectsList() {
    this.projectService.getAll().subscribe(value => {
      this.projects = value;
    });
  }
}

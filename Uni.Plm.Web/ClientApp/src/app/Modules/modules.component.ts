import { Component, OnInit } from '@angular/core';
import {Module} from '../Shared/_models/module.model';
import {ModuleService} from '../Shared/_services/module.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

  modules: Module[];
  modalRef: any;

  constructor(private moduleService: ModuleService, private modalService: NgbModal) { }

  ngOnInit() {
    this.updateModulesList();
  }

  open(content) {
    this.modalRef = this.modalService.open(content);
  }

  onModuleAdded() {
    this.modalRef.close();
    this.updateModulesList();
  }

  updateModulesList() {
    this.moduleService.getAll().subscribe(value => this.modules = value);
  }

  deleteModule(id: string) {
    this.moduleService.deleteModule(id).subscribe(() => {
      setTimeout(() => {
        this.updateModulesList();
      }, 500);
    });
  }

}

import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ModuleService} from '../Shared/_services/module.service';
import {ModulesComponent} from './modules.component';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import { NewModuleComponent } from './new-module/new-module.component';

@NgModule({
  declarations: [
    ModulesComponent,
    NewModuleComponent,

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    FormsModule,
    NgbModule.forRoot()
  ],
  exports: [
  ],
  providers: [
    ModuleService
  ]
})
export class ModulesModule { }

import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ModuleService} from '../../Shared/_services/module.service';
import {Module} from '../../Shared/_models/module.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-new-module',
  templateUrl: './new-module.component.html',
  styleUrls: ['./new-module.component.css']
})
export class NewModuleComponent implements OnInit {

  displayName: string;
  description: string;
  @ViewChild('fileInput') file: ElementRef;
  @Input('closeBtn') closeBtn: ElementRef;
  @Output() uploaded = new EventEmitter<string>();

  constructor(private moduleService: ModuleService, private modalService: NgbModal) { }

  ngOnInit() {
  }

  addNew() {
    const module = new Module();
    module.displayName = this.displayName;
    module.description = this.description;
    module.file = this.file.nativeElement.files[0];
    this.moduleService.add(module).subscribe(() => {

    }, error => {
      console.error(error);
    }, () => {
      setTimeout(() => {
        this.uploaded.emit('complete');
      }, 500);
    });
  }

}

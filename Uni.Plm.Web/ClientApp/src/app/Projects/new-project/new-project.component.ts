import { Component, OnInit } from '@angular/core';
import {ProjectsService} from '../../Shared/_services/projects.service';
import {Project} from '../../Shared/_models/project.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent implements OnInit {

  displayName: string;
  description: string;

  constructor(private projectService: ProjectsService, private router: Router) { }

  ngOnInit() {
  }

  addNew() {

    const newProject = new Project();
    newProject.displayName = this.displayName;
    newProject.description = this.description;

    this.projectService.add(newProject).subscribe(project => {
      this.router.navigate(['projects', project.id, 'settings']);
    });
  }

}

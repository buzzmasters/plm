import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../Shared/_models/user.model';
import {catchError, debounceTime, distinctUntilChanged, merge, switchMap, tap} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {UsersService} from '../../../Shared/_services/users.service';
import {ProjectsService} from '../../../Shared/_services/projects.service';
import {of} from 'rxjs/observable/of';
import {Module} from '../../../Shared/_models/module.model';
import {ModuleService} from '../../../Shared/_services/module.service';

@Component({
  selector: 'app-modules-settings',
  templateUrl: './modules-settings.component.html',
  styleUrls: ['./modules-settings.component.css']
})
export class ModulesSettingsComponent implements OnInit {

  @Input() projectId: string;
  modules: Module[];
  model: any;
  searching = false;
  searchFailed = false;
  hideSearchingWhenUnsubscribed = new Observable(() => () => this.searching = false);

  formatter = (x: {
    username: string,
    email: string
  }) => x.username;

  constructor(private moduleService: ModuleService, private projectsService: ProjectsService) { }

  ngOnInit() {
    this.updateModulesList();
  }

  searchModule = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term =>
        this.moduleService.search(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          }))
      ),
      tap(() => this.searching = false),
      merge(this.hideSearchingWhenUnsubscribed)
    );

  selectModule(module: Module) {

    this.projectsService.addModule(this.projectId, module.id).subscribe(() => {
      this.updateModulesList();
    });
  }

  updateModulesList() {
    this.moduleService.getModulesByProjectId(this.projectId)
      .subscribe(modules => this.modules = modules);
  }

  deleteModuleFromProject(moduleId: string, projectId: string) {
    this.projectsService.deleteModule(projectId, moduleId).subscribe(() => {
      this.updateModulesList();
    });
  }

}

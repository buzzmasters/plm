import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, ActivatedRouteSnapshot} from '@angular/router';
import {ProjectsService} from '../../Shared/_services/projects.service';
import {of} from 'rxjs/observable/of';
import {UsersService} from '../../Shared/_services/users.service';
import {catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, merge} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {NgbTypeaheadSelectItemEvent} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../../Shared/_models/user.model';

@Component({
  selector: 'app-project-settings',
  templateUrl: './project-settings.component.html',
  styleUrls: ['./project-settings.component.css']
})
export class ProjectSettingsComponent implements OnInit {

  generalForm: FormGroup;
  projectId: string;


  constructor(private fb: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private projectService: ProjectsService,
              private usersService: UsersService) {
    this.createGeneralForm();
    this.projectId = activatedRoute.snapshot.params.projectId;
  }

  ngOnInit() {
    this.projectService.get(this.projectId)
      .subscribe(p => {
        this.generalForm.patchValue({
          displayName: p.displayName,
          description: p.description
        });
      });
  }

  saveGeneral() {

  }

  createGeneralForm() {
    this.generalForm = this.fb.group({
      displayName: ['', Validators.required],
      description: ['', Validators.required]
    });
  }


}

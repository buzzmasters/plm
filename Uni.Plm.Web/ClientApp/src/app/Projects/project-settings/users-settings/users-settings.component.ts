import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {User} from '../../../Shared/_models/user.model';
import {catchError, debounceTime, distinctUntilChanged, merge, switchMap, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {UsersService} from '../../../Shared/_services/users.service';
import {ProjectsService} from '../../../Shared/_services/projects.service';

@Component({
  selector: 'app-users-settings',
  templateUrl: './users-settings.component.html',
  styleUrls: ['./users-settings.component.css']
})
export class UsersSettingsComponent implements OnInit {

  @Input() projectId: string;
  users: User[];
  model: any;
  searching = false;
  searchFailed = false;
  hideSearchingWhenUnsubscribed = new Observable(() => () => this.searching = false);

  formatter = (x: {
    username: string,
    email: string
  }) => x.username;

  constructor(private usersService: UsersService, private projectsService: ProjectsService) { }

  ngOnInit() {
    this.updateUsersList();
  }

  searchUser = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term =>
        this.usersService.search(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          }))
      ),
      tap(() => this.searching = false),
      merge(this.hideSearchingWhenUnsubscribed)
    );

  selectUser(user: User) {

    this.projectsService.addUser(this.projectId, user.id).subscribe(() => {
      this.updateUsersList();
    });
  }

  updateUsersList() {
    this.usersService.getUsersByProjectId(this.projectId)
      .subscribe(users => this.users = users);
  }

  deleteUserFromProject(userId: number, projectId: string) {
    this.projectsService.deleteUser(projectId, userId).subscribe(() => {
      this.updateUsersList();
    });
  }

}

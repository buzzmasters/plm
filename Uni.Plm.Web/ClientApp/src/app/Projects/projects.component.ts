import { Component, OnInit } from '@angular/core';
import {Project} from '../Shared/_models/project.model';
import {ProjectsService} from '../Shared/_services/projects.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projects: Project[] = [];

  constructor(private projectService: ProjectsService) { }

  ngOnInit() {
    this.updateProjectsList();
  }

  deleteProject(id: string) {
    this.projectService.delete(id).subscribe(() => {
      this.updateProjectsList();
    });
  }

  updateProjectsList() {
    this.projectService.getAll().subscribe(value => {
      this.projects = value;
    });
  }

}

import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProjectsComponent} from './projects.component';
import {ProjectsService} from '../Shared/_services/projects.service';
import { NewProjectComponent } from '../Projects/new-project/new-project.component';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {NgSelectModule} from '@ng-select/ng-select';
import { ProjectSettingsComponent } from '../Projects/project-settings/project-settings.component';
import { UsersSettingsComponent } from './project-settings/users-settings/users-settings.component';
import { ModulesSettingsComponent } from '../Projects/project-settings/modules-settings/modules-settings.component';
import {ModuleService} from '../Shared/_services/module.service';
import {UsersService} from '../Shared/_services/users.service';


@NgModule({
  declarations: [
    ProjectsComponent,
    NewProjectComponent,
    ProjectSettingsComponent,
    UsersSettingsComponent,
    ModulesSettingsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    FormsModule,
    RouterModule,
    NgSelectModule,
    ReactiveFormsModule,

    NgbModule.forRoot()
  ],
  exports: [
  ],
  providers: [
    ProjectsService,
    ModuleService,
    UsersService
  ]
})
export class ProjectsModule { }

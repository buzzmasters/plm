import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../_services/auth.service';
import {User} from '../../_models/user.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss'],
  selector: 'shared-header'
})

export class HeaderComponent {

  loggedUsername: string;
  username: string;
  password: string;

  constructor(private _authService: AuthService, private modalService: NgbModal) {
    this.loggedUsername = _authService.isLogged();
  }

  isLogged(): boolean {
    return this.loggedUsername != null;
  }

  login() {
    const loginInfo = new User();
    loginInfo.username = this.username;
    loginInfo.password = this.password;

    this._authService.login(loginInfo).subscribe(() => {
      location.href = location.href;
    });
  }

  logout() {
    this._authService.logout();
  }

  open(content) {
    this.modalService.open(content);
  }
}

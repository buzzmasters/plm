import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../_services/auth.service';
import {User} from '../../../_models/user.model';

@Component({
  selector: 'register-modal',
  templateUrl: 'register-modal.component.html'
})

export class RegisterModalComponent {


  constructor(private _authService: AuthService) {  }


  public username: string;
  public password: string;
  public email: string;


  register() {
    const regUser = new User();
    regUser.username = this.username;
    regUser.password = this.password;
    regUser.email = this.email;

    this._authService.register(regUser).subscribe(() => {
      this._authService.login(regUser).subscribe(() => {
        location.href = location.href;
      });
    });
  }
}

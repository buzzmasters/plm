export class Module {
  id: string;
  displayName: string;
  file: any;
  name: string;
  description: string;
}

export class ProjectGroup {
  id: number;
  name: string;
  description: string;
}

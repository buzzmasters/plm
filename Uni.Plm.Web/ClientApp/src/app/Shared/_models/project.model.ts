export class Project {
  id: string;
  displayName: string;
  description: string;
}

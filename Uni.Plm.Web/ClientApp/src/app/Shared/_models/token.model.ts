export class Token {
  access_token: string;
  token_type: string;
  username: string;
  '.issued': Date;
  '.expires': Date;
  expires_in: number;
}

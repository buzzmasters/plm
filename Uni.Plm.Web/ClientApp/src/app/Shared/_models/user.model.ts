import {ProjectGroup} from './project-group';

export class User {
  id: number;
  email: string;
  password: string;
  username: string;
  projectGroup: ProjectGroup;
}

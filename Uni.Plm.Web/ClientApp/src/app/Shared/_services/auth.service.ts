import {Inject, Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { HttpClient } from '@angular/common/http';

import { CookieService } from 'angular2-cookie/services/cookies.service';

import { User } from '../_models/user.model';
import { Token } from '../_models/token.model';
import {AUTH_URL, WEB_URL} from '../constants';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient,
              private cookieService: CookieService,
              @Inject(AUTH_URL) private authUrl: string,
              private router: Router) {  }

  register(user: User): Observable<any> {
    return this.http.post(`${this.authUrl}register`, user);
  }

  login(user: User): Observable<Token> {

    const token: Observable<Token> = this.http.post<Token>(`${this.authUrl}login`,
      {
        username: user.username,
        password: user.password,
        grant_type: 'password'
      }, {})
      .do(tokenResponse => {

        this.cookieService.put('UniPlmToken', tokenResponse.access_token, {
          expires: tokenResponse['.expires']
        });
        this.cookieService.put('UniPlmUsername', tokenResponse.username, {
          expires: tokenResponse['.expires']
        });

        return tokenResponse;
      });

    return token;
  }

  logout() {
    this.cookieService.remove('UniPlmToken');
    this.cookieService.remove('UniPlmUsername');
    this.router.navigateByData({
      url: ["/"],
      data: {is401: true}
    });
  }

  getToken(): string {
    const token = this.cookieService.get('UniPlmToken');
    return token;
  }

  getName(): string {
    const userName = this.cookieService.get("UniPlmUsername");
    return userName;
  }

  isLogged(): string {
    const userName = this.cookieService.get("UniPlmUsername");
    const token = this.cookieService.get("UniPlmToken");

    return userName && token ? userName : null;
  }
}

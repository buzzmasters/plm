import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot } from '@angular/router';
import {ModuleService} from './module.service';
import {ProjectsService} from './projects.service';

@Injectable()
export class CurrentStepResolver implements Resolve<string> {
  constructor(private projectsService: ProjectsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<string> {

    return this.projectsService.getCurrentStepName(route.params.projectId);
  }
}

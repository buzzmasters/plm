import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {WEB_URL} from '../constants';
import {Module} from '../_models/module.model';
import {User} from '../_models/user.model';
import {of} from 'rxjs/observable/of';
import {Project} from '../_models/project.model';

@Injectable()
export class ModuleService {

  constructor(private http: HttpClient, @Inject(WEB_URL) private webUrl: string) { }

  getCurrentStep(): Observable<string> {
    const options: Object = {responseType: 'text' as 'text'};
    return this.http.get<string>(`${this.webUrl}api/module/GetCurrentStepName`, options)
      .map(value => value);
  }

  add(module: Module): Observable<any> {
    const formData = new FormData();
    formData.append('displayName', module.displayName);
    formData.append('description', module.description);
    formData.append('file', module.file);
    return this.http.post(`${this.webUrl}api/module`, formData);
  }

  getAll(): Observable<Module[]> {
    return this.http.get<Module[]>(`${this.webUrl}api/module`);
  }

  deleteModule(id: string): Observable<any> {
    return this.http.delete(`${this.webUrl}api/module/${id}`);
  }

  getModulesByProjectId(id: string): Observable<Module[]> {
    return this.http.get<Module[]>(`${this.webUrl}api/module/${id}`);
  }

  search(term: string): Observable<Module[]> {

    if (term == '') {
      return of([]);
    }

    return this.http.get<Module[]>(`${this.webUrl}api/module`, {
      params: {
        term
      }
    });
  }
}


import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {WEB_URL} from '../constants';
import {Project} from '../_models/project.model';
import {Module} from '../_models/module.model';

@Injectable()
export class ProjectsService {

  constructor(private http: HttpClient, @Inject(WEB_URL) private webUrl: string) { }

  getAll(): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.webUrl}api/projects`);
  }

  get(id: string): Observable<Project> {
    return this.http.get<Project>(`${this.webUrl}api/projects/${id}`);
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${this.webUrl}api/projects/${id}`);
  }

  add(project: Project): Observable<Project> {
    return this.http.post<Project>(`${this.webUrl}api/projects`, project);
  }

  addUser(projectId: string, userId: number): Observable<any> {
    return this.http.post(`${this.webUrl}api/projects/user`, {projectId, userId});
  }

  deleteUser(projectId: string, userId: number): Observable<any> {
    const params = new HttpParams()
      .set("projectId", projectId)
      .set("userId", userId.toString());
    return this.http.delete(`${this.webUrl}api/projects/user`, {params});
  }

  addModule(projectId: string, moduleId: string): Observable<any> {
    return this.http.post(`${this.webUrl}api/projects/module`, {
      projectId: projectId,
      moduleId: moduleId
    });
  }

  deleteModule(projectId: string, moduleId: string): Observable<any> {
    return this.http.delete(`${this.webUrl}api/projects/module`, {params: {
        projectId: projectId,
        moduleId: moduleId
      }});
  }

  getCurrentStepName(projectId: string) {
    const options: Object = {responseType: 'text' as 'text'};
    return this.http.get<string>(`${this.webUrl}api/projects/${projectId}/step/name`, options)
      .map(value => value.substring(1, value.length - 1));
  }

  getSteps(projectId: string): Observable<Module[]> {
    return this.http.get<Module[]>(`${this.webUrl}api/projects/${projectId}/workflow/steps`);
  }
}


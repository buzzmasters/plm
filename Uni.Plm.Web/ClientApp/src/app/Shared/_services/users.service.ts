import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {WEB_URL} from '../constants';
import {User} from '../_models/user.model';
import {of} from 'rxjs/observable/of';

@Injectable()
export class UsersService {

  constructor(private http: HttpClient, @Inject(WEB_URL) private webUrl: string) { }

  search(term: string): Observable<User[]> {

    if (term == '') {
      return of([]);
    }

    return this.http.get<User[]>(`${this.webUrl}api/user`, {
      params: {
        term
      }
    });
  }

  getUsersByProjectId(id: string): Observable<User[]> {
    return this.http.get<User[]>(`${this.webUrl}api/user/${id}`);
  }
}


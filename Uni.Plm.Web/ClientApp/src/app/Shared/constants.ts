import {InjectionToken} from '@angular/core';

export const WEB_URL = new InjectionToken<string>('webUrl');
export const AUTH_URL = new InjectionToken<string>('authUrl');

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import {AuthService} from "./_services/auth.service";
import {HeaderComponent} from './_components/Header/header.component';
import {RegisterModalComponent} from './_components/Header/register-modal/register-modal.component';
import {RouterModule} from '@angular/router';
import {CurrentStepResolver} from './_services/cycle-step-resolver.service';
import {ModuleService} from './_services/module.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from './_interceptors/token.interceptor';
import {ProjectsService} from './_services/projects.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {UnauthorizedInterceptor} from './_interceptors/unauthorized.interceptor';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import "angular2-navigate-with-data";
import {FooterComponent} from './_components/Footer/footer.component';
import {UsersService} from './_services/users.service';
@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    RegisterModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    AngularFontAwesomeModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent
  ],
  providers: [
    AuthService,
    CurrentStepResolver,
    ModuleService,
    ProjectsService,
    UsersService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnauthorizedInterceptor,
      multi: true
    }
  ]
})
export class SharedModule { }

import {Component, Inject, OnInit} from '@angular/core';
import {ModuleService} from '../Shared/_services/module.service';
import {ActivatedRoute, ActivatedRouteSnapshot} from '@angular/router';
import {WEB_URL} from '../Shared/constants';
import {ProjectsService} from '../Shared/_services/projects.service';
import {Module} from '../Shared/_models/module.model';

@Component({
  templateUrl: './cycle.component.html',
  styleUrls: ['./cycle.component.css']
})
export class CycleComponent implements OnInit {

  moduleUrl = '';
  projectId = '';
  steps: Module[] = [];
  currentStep: Module;
  isFirstStep = true;
  isLastStep = true;

  constructor(private moduleService: ModuleService,
              private projectsService: ProjectsService,
              private route: ActivatedRoute,
              private activatedRoute: ActivatedRoute,
              @Inject(WEB_URL) private webUrl: string) {

  }

  ngOnInit(): void {

    this.projectId = this.activatedRoute.snapshot.params.projectId;
    this.projectsService.getSteps(this.projectId)
      .subscribe(s => {

        this.steps = s;

        this.route.data
          .subscribe((data: {step: string}) => {
            this.moduleUrl = `${this.webUrl}${data.step}/Module?projectId=${this.projectId}`;
            this.currentStep = this.steps.find(step => step.name == data.step);

            this.validateStepIndex();
          });

      });
  }

  toPrevious() {
    let index;
    this.steps.find((e, i) => {
      const isEq = e.id == this.currentStep.id;
      if (isEq) { index = i; }
      return isEq;
    });

    this.currentStep = this.steps[index - 1];
    this.moduleUrl = `${this.webUrl}${this.currentStep.name}/Module?projectId=${this.projectId}`;
    this.validateStepIndex();
  }

  toNext() {
    let index;
    this.steps.find((e, i) => {
      const isEq = e.id == this.currentStep.id;
      if (isEq) { index = i; }
      return isEq;
    });

    this.currentStep = this.steps[index + 1];
    this.moduleUrl = `${this.webUrl}${this.currentStep.name}/Module?projectId=${this.projectId}`;
    this.validateStepIndex();
  }

  private getCurrentStepIndex() {
    let index;
    this.steps.find((e, i) => {
      const isEq = e.id == this.currentStep.id;
      if (isEq) { index = i; }
      return isEq;
    });

    return index;
  }

  private validateStepIndex() {
    const i = this.getCurrentStepIndex();
    if (i == 0) {
      this.isFirstStep = true;
      this.isLastStep = false;
    } else if (i == this.steps.length - 1) {
      this.isFirstStep = false;
      this.isLastStep = true;
    }
  }
}

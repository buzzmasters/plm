import { BrowserModule } from '@angular/platform-browser';
import {InjectionToken, NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {ActivatedRouteSnapshot, RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import {HomeComponent} from './Home/home.component';
import {CycleComponent} from './Workflow/cycle.component';
import {environment} from '../environments/environment';
import {AUTH_URL, WEB_URL} from './Shared/constants';
import {CurrentStepResolver} from './Shared/_services/cycle-step-resolver.service';
import {ModuleService} from './Shared/_services/module.service';
import {SafePipe} from './Shared/_pipes/safe.pipe';
import {CookieService} from 'angular2-cookie';
import {SharedModule} from './Shared/shared.module';
import { ProjectsComponent } from './Projects/projects.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModulesComponent } from './Modules/modules.component';
import {ModulesModule} from './Modules/modules.module';
import {ProjectsModule} from './Projects/projects.module';
import {NewProjectComponent} from './Projects/new-project/new-project.component';
import {ProjectSettingsComponent} from './Projects/project-settings/project-settings.component';

export function cookieServiceFactory() {
  return new CookieService();
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CycleComponent,

    SafePipe
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'projects', component: ProjectsComponent, pathMatch: 'full'},
      { path: 'projects/create', component: NewProjectComponent, pathMatch: 'full'},
      { path: 'projects/:projectId/settings', component: ProjectSettingsComponent, pathMatch: 'full'},
      { path: ':projectId/workflow', component: CycleComponent, pathMatch: 'full', resolve: {step: CurrentStepResolver }},
      { path: 'modules', component: ModulesComponent, pathMatch: 'full'},

    ]),
    SharedModule,
    ModulesModule,
    ProjectsModule,
    NgbModule.forRoot()
  ],
  providers: [
    {provide: WEB_URL, useValue: environment.webUrl},
    {provide: AUTH_URL, useValue: environment.authUrl},
    { provide: CookieService, useFactory: cookieServiceFactory },
    ModuleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

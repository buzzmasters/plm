﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Uni.Plm.Web.Entities;
using Uni.Plm.Web.ViewModels;

namespace Uni.Plm.Web.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/module")]
    public class ModuleController : Controller
    {
        private readonly PlmContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly string _uploadPath = SiteConfiguration.PathToModules;

        private IApplicationLifetime ApplicationLifetime { get; set; }

        public ModuleController(PlmContext context, IHostingEnvironment hostingEnvironment, IApplicationLifetime applicationLifetime)
        {
            ApplicationLifetime = applicationLifetime;
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("")]
        public async Task AddModule([FromForm]ModuleViewModel module)
        {
            string webRootPath = _hostingEnvironment.WebRootPath;
            string uploadPath = Path.Combine(webRootPath, _uploadPath);
            if (!Directory.Exists(uploadPath))
            {
                Directory.CreateDirectory(uploadPath);
            }

            if (module.File.Length > 0)
            {
                string fileName = ContentDispositionHeaderValue.Parse(module.File.ContentDisposition).FileName.Trim('"');
                string fullPath = Path.Combine(uploadPath, fileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    await module.File.CopyToAsync(stream);
                }
            }

            _context.Modules.Add(new Module()
            {
                Id = Guid.NewGuid(),
                Name = Path.GetFileNameWithoutExtension(module.File.FileName),
                Description = module.Description,
                DisplayName = module.DisplayName
            });

            await _context.SaveChangesAsync();

            ApplicationLifetime.StopApplication();
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<Module> GetAll()
        {
            try
            {
                return _context.Modules.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task RemoveModule(Guid id)
        {
            var moduleEntity = _context.Modules.First(module => module.Id.Equals(id));
            _context.Modules.Remove(moduleEntity);

            await _context.SaveChangesAsync();

            string webRootPath = _hostingEnvironment.WebRootPath;
            string uploadPath = Path.Combine(webRootPath, _uploadPath);

            //TODO: The assembly cannot be loaded into custom domain so it could be unloaded until Core 2.2.
            //https://github.com/dotnet/core/issues/1339
            //System.IO.File.Delete(Path.Combine(uploadPath, moduleEntity.Name + ".dll")); 

            ApplicationLifetime.StopApplication();
        }

        public IEnumerable<ModuleViewModel> SearchForModules(string term)
        {
            return _context.Modules
                .Where(module =>
                    CultureInfo.InvariantCulture.CompareInfo.IndexOf(module.DisplayName, term, CompareOptions.IgnoreCase) >= 0)
                .Select(Mapper.Map<ModuleViewModel>);
        }

        [HttpGet]
        [Route("{projectId}")]
        public IEnumerable<ModuleViewModel> GetModulesByProjectId(Guid projectId)
        {
            return _context.ExecSql<Module>($"GetModulesByProjectId '{projectId}'")
                .Select(m => new ModuleViewModel()
                {
                    Id = m.Id,
                    Description = m.Description,
                    DisplayName = m.DisplayName
                });
        }
    }
}
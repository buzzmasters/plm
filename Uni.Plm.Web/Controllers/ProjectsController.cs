﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Uni.Plm.Web.Entities;
using Uni.Plm.Web.ViewModels;

namespace Uni.Plm.Web.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/projects")]
    public class ProjectsController : Controller
    {
        private readonly PlmContext _context;

        public ProjectsController(PlmContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<ProjectViewModel> GetAllProjects()
        {
            int userId = int.Parse(User.Claims.First(claim => claim.Type == "UserId").Value);
            return _context.GetProjectsForUser(userId).Select(Mapper.Map<ProjectViewModel>).ToList();
        }

        [HttpGet]
        [Route("{id}")]
        public ProjectViewModel GetById(Guid id)
        {
            int userId = int.Parse(User.Claims.First(claim => claim.Type == "UserId").Value);
            var project = _context.GetProjectsForUser(userId).FirstOrDefault(p => p.Id.Equals(id));

            return project == null
                ? null
                : Mapper.Map<ProjectViewModel>(project);
        }

        [HttpDelete]
        [Route("{id}")]
        public void DeleteById(Guid id)
        {
            try
            {
                _context.DeleteProjectById(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Route("")]
        public ProjectViewModel CreateProject([FromBody] ProjectViewModel projectViewModel)
        {
            int userId = int.Parse(User.Claims.First(claim => claim.Type == "UserId").Value);
            var project = _context.AddProject(userId, projectViewModel.DisplayName, projectViewModel.Description);

            return Mapper.Map<ProjectViewModel>(project);
        }

        [HttpPost]
        [Route("user")]
        public void AddUserToProject([FromBody] ProjectUserViewModel projectUserViewModel)
        {
            _context.AddUserToProject(projectUserViewModel.UserId, projectUserViewModel.ProjectId);
        }

        [HttpDelete]
        [Route("user")]
        public void DeleteUserFromProject(Guid projectId, int userId)
        {
            _context.RemoveUserFromProject(userId, projectId);
        }

        [HttpPost]
        [Route("module")]
        public void AddModuleToProject([FromBody] ProjectModuleViewModel projectModuleViewModel)
        {
            _context.Database.ExecuteSqlCommand($"AddModuleToProject @p0, @p1", projectModuleViewModel.ProjectId, projectModuleViewModel.ModuleId);
        }

        [HttpDelete]
        [Route("module")]
        public void DeleteModuleFromProject(Guid projectId, Guid moduleId)
        {
            _context.Database.ExecuteSqlCommand($"DeleteModuleFromProject @p0, @p1", projectId, moduleId);
        }

        [HttpGet]
        [Route("{projectId}/step")]
        public Guid GetCurrentStep(Guid projectId)
        {
            return _context.ExecSqlSingleResult<Guid>($"GetCurrentStepByProjectId '{projectId}'");
        }

        [HttpGet]
        [Route("{projectId}/step/name")]
        public string GetCurrentStepName(Guid projectId)
        {
            return _context.ExecSqlSingleResult<string>($"GetCurrentStepNameByProjectId '{projectId}'");
        }

        [HttpGet]
        [Route("{projectId}/workflow/steps")]
        public IEnumerable<Module> GetSteps(Guid projectId)
        {
            return _context.ExecSql<Module>($"GetStepsByProjectId '{projectId}'");
        }
    }
}
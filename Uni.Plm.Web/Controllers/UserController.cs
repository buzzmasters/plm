﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Uni.Plm.Web.Entities.Custom;
using Uni.Plm.Web.ViewModels;

namespace Uni.Plm.Web.Controllers
{
    [Produces("application/json")]
    [Authorize]
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly PlmContext _context;

        public UserController(PlmContext context)
        {
            _context = context;
        }

        public IEnumerable<UserViewModel> SearchForUsers(string term)
        {
            return _context.Users
                .Where(user => 
                    user.Email.StartsWith(term, StringComparison.InvariantCultureIgnoreCase))
                .Select(Mapper.Map<UserViewModel>);
        }

        [HttpGet]
        [Route("{projectId}")]
        public IEnumerable<UserViewModel> GetUsersByProjectId(Guid projectId)
        {
            try
            {
                return _context.ExecSql<UserExtended>($"GetUsersByProjectId '{projectId}'")
                    .Select(u => new UserViewModel()
                    {
                        Id = u.Id,
                        Username = u.Username,
                        Email = u.Email,
                        ProjectGroup = new ProjectGroup()
                        {
                            Id = u.ProjectGroupId,
                            Name = u.ProjectGroupName,
                            Description = u.ProjectGroupDescription
                        }
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
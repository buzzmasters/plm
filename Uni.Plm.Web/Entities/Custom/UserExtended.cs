﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uni.Plm.Web.Entities.Custom
{
    public class UserExtended
    {
        public int? Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }
        
        public int ProjectGroupId { get; set; }

        public string ProjectGroupName { get; set; }

        public string ProjectGroupDescription { get; set; }
    }
}

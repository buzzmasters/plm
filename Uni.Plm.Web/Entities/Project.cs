﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uni.Plm.Web.Entities
{
    public class Project
    {
        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public Guid? CurrentStepId { get; set; }
    }
}

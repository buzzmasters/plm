﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Uni.Plm.Common;

namespace Uni.Plm.Web.Helpers
{
    public class AssemblyHelper
    {
        public static Type GetMainModuleComponent(string assemblyName)
        {
            var asm = AppDomain.CurrentDomain.GetAssemblies().First(assembly => assembly.GetName().Name.Equals(assemblyName, StringComparison.OrdinalIgnoreCase));
            var type = asm.GetTypes()
                .FirstOrDefault(t => t.GetCustomAttributes(typeof(ModuleComponentAttribute), true).Length > 0);

            return type;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Uni.Plm.Common.Configs.Application;
using Uni.Plm.Common.Configs.Input;

namespace Uni.Plm.Web.Helpers
{
    public class EmbeddedResourceHelper
    {
        private readonly Assembly _assembly;

        public EmbeddedResourceHelper(string pathToLibrary)
        {
            _assembly = Assembly.LoadFrom(pathToLibrary);
        }

        public ApplicationConfig GetApplicationConfig()
        {
            string configString = GetStringResource(_assembly, "config.json");

            return JsonConvert.DeserializeObject<ApplicationConfig>(configString);
        }

        public IEnumerable<InputParameter> GetInputParameters()
        {
            string inputsString = GetStringResource(_assembly, "inputs.json");

            return JsonConvert.DeserializeObject<IEnumerable<InputParameter>>(inputsString, new JsonSerializerSettings(){NullValueHandling = NullValueHandling.Ignore});
        }

        public string GetPageContent(string pagePath)
        {
            return GetStringResource(_assembly, pagePath);
        }

        private static string GetStringResource(Assembly assembly, string resourcePath)
        {
            Stream stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.{resourcePath}");
            StreamReader reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uni.Plm.Auth.Model;
using Uni.Plm.Web.Entities;
using Module = Uni.Plm.Web.Entities.Module;

namespace Uni.Plm.Web
{
    public class PlmContext: DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<User> Users { get; set; }

        public PlmContext(DbContextOptions<PlmContext> options)
            : base(options)
        { }

        public PlmContext(string connectionString) : this(GetOptions(connectionString))
        {
        }

        private static DbContextOptions<PlmContext> GetOptions(string connectionString)
        {
            return new DbContextOptionsBuilder<PlmContext>().UseSqlServer(connectionString).Options;
        }

        public IEnumerable<Project> GetProjectsForUser(int userId)
        {
            return this.ExecSql<Project>($"GetProjectsForUser {userId}");
        }

        public void DeleteProjectById(Guid projectId)
        {
            this.Database.ExecuteSqlCommand("DeleteProjectById @p0", projectId);
        }

        public Project AddProject(int userId, string name, string description)
        {
            return this.ExecSql<Project>($"AddProject {userId}, '{name}', '{description}'").First();
        }

        public void AddUserToProject(int userId, Guid projectId)
        {
            this.Database.ExecuteSqlCommand("AddUserToProject @p0, @p1", projectId, userId);
        }

        public void RemoveUserFromProject(int userId, Guid projectId)
        {
            this.Database.ExecuteSqlCommand("DeleteUserFromProject @p0, @p1", projectId, userId);
        }

        public List<T> ExecSql<T>(string query)
        {
            using (var command = Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                Database.OpenConnection();

                using (var result = command.ExecuteReader())
                {
                    List<T> list = new List<T>();
                    T obj = default(T);
                    while (result.Read())
                    {
                        obj = Activator.CreateInstance<T>();
                        foreach (PropertyInfo prop in obj.GetType().GetProperties())
                        {
                            if (!object.Equals(result[prop.Name], DBNull.Value))
                            {
                                prop.SetValue(obj, result[prop.Name], null);
                            }
                        }
                        list.Add(obj);
                    }

                    return list;
                }
            }
        }

        public T ExecSqlSingleResult<T>(string query)
        {
            using (var command = Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                Database.OpenConnection();

                using (var result = command.ExecuteReader())
                {
                    List<T> list = new List<T>();
                    T obj = default(T);
                    result.Read();
                    obj = (T) result[0];

                    return obj;
                }
            }
        }
    }
}

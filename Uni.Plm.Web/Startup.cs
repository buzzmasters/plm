using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Uni.Plm.Auth.Model;
using Uni.Plm.Common;
using Uni.Plm.Web.Entities;
using Uni.Plm.Web.ViewModels;

namespace Uni.Plm.Web
{
    public class Startup
    {
        private const string PathToModulesKey = "PathToModules";
        private const string PlmDatabaseKey = "PlmDatabase";

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(o => {
                    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        // ��������, ����� �� �������������� �������� ��� ��������� ������
                        ValidateIssuer = false,
                        // ������, �������������� ��������
                        //ValidIssuer = AuthOptions.ISSUER,

                        // ����� �� �������������� ����������� ������
                        ValidateAudience = false,
                        // ��������� ����������� ������
                        //ValidAudience = AuthOptions.AUDIENCE,
                        // ����� �� �������������� ����� �������������
                        ValidateLifetime = true,

                        // ��������� ����� ������������
                        IssuerSigningKey = GetSymmetricSecurityKey(),
                        // ��������� ����� ������������
                        ValidateIssuerSigningKey = true,
                    };
                });

            var mvcBuilder = services.AddMvc();
            services.AddCors();
            SiteConfiguration.PathToModules = Configuration.GetSection(PathToModulesKey).Value;
            SiteConfiguration.PlmDatabaseConnectionString = Configuration.GetConnectionString(PlmDatabaseKey);
            services.AddDbContext<PlmContext>(options => options.UseSqlServer(SiteConfiguration.PlmDatabaseConnectionString));
            services.AddDbContext<PlmCommonContext>(options => options.UseSqlServer(SiteConfiguration.PlmDatabaseConnectionString));
            IEnumerable<Assembly> assemblies = GetModulesAssemblies(SiteConfiguration.PathToModules);
            foreach (var assembly in assemblies)
            {
                mvcBuilder.AddApplicationPart(assembly);
            }

            var fileProviders =
                assemblies.Select(a => new EmbeddedFileProvider(a, a.GetName().Name));
            
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.FileProviders.Add(new CompositeFileProvider(fileProviders));
            });

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
        }

        static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes("mysupersecret_secretkey!123"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            InitializeAutomapper();

            app.UseAuthentication();

            app.UseCors(builder =>
                builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin()
            );

            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areas",
                    template: "{area}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }

        private IEnumerable<Assembly> GetModulesAssemblies(string pathToModules)
        {
            string absolutePath = Path.Combine(Environment.WebRootPath, pathToModules);

            if (!Directory.Exists(absolutePath))
            {
                Directory.CreateDirectory(absolutePath);
            }

            // This block is for removing unused modules on startup, 
            // since dynamic unloading of dlls is not yet supported and it cannot be done in REMOVE method.
            // https://github.com/dotnet/core/issues/1339
            using (var ctx = new PlmContext(SiteConfiguration.PlmDatabaseConnectionString))
            {
                var modules = ctx.Modules.ToList();

                foreach (var file in Directory.GetFiles(absolutePath))
                {
                    if (!modules.Any(module => module.Name.Equals(Path.GetFileNameWithoutExtension(file))))
                    {
                        File.Delete(file);
                    }
                }
            }

            var asmPaths = Directory.GetFiles(absolutePath)
                .Where(s => s.EndsWith(".dll", StringComparison.OrdinalIgnoreCase)).ToList();

            // TODO: Load into new domain when Core 2.2 is out
            // https://github.com/dotnet/core/issues/1339
            return asmPaths.Select(Assembly.LoadFrom);
        }

        private void InitializeAutomapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Project, ProjectViewModel>();
                cfg.CreateMap<User, UserViewModel>();
                cfg.CreateMap<ProjectViewModel, Project>();
            });
        }
    }
}

﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace Uni.Plm.Web.ViewModels
{
    public class ModuleViewModel
    {
        public Guid? Id { get; set; }
        
        public string DisplayName { get; set; }
        
        public IFormFile File { get; set; }
        
        public string Description { get; set; }
    }
}

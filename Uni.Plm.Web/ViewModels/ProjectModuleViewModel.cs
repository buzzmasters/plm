﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uni.Plm.Web.ViewModels
{
    public class ProjectModuleViewModel
    {
        public Guid ModuleId { get; set; }
        public Guid ProjectId { get; set; }
    }
}

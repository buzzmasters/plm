﻿using System;

namespace Uni.Plm.Web.ViewModels
{
    public class ProjectViewModel
    {
        public Guid? Id { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public Guid? CurrentStepId { get; set; }
    }
}

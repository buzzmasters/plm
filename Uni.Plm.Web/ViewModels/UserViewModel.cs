﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uni.Plm.Web.ViewModels
{
    public class UserViewModel
    {
        public int? Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public ProjectGroup ProjectGroup { get; set; }
    }

    public class ProjectGroup
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Uni.Plm.Web.Controllers
{
    public class ModuleController : Controller
    {
        private static int CurrentStep = 0;

        public IActionResult Index()
        {
            return View("Index", _steps[CurrentStep]);
        }

        public string Next()
        {
            if (CurrentStep < _steps.Count-1)
                CurrentStep++;

            return _steps[CurrentStep];
        }

        public string Prev()
        {
            if (CurrentStep > 0)
                CurrentStep--;

            return _steps[CurrentStep];
        }

        public int GetCurrentStepId()
        {
            return CurrentStep;
        }

        private Dictionary<int, string> _steps = new Dictionary<int, string>()
        {
            { 0, "Uni.Plm.Test0.Module" },
            { 1, "Uni.Plm.Test1.Module" }
        };
    }
}
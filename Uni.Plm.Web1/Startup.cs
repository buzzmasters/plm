﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyModel;
using Microsoft.Extensions.DependencyModel.Resolution;
using Microsoft.Extensions.FileProviders;

namespace Uni.Plm.Web
{
    public class Startup
    {
        private const string PathToModulesKey = "PathToModules";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var mvcBuilder = services.AddMvc();

            SiteConfiguration.PathToModules = Configuration.GetSection(PathToModulesKey).Value;

            IEnumerable<Assembly> assemblies = GetModulesAssemblies(SiteConfiguration.PathToModules);
            foreach (var assembly in assemblies)
            {
                mvcBuilder.AddApplicationPart(assembly);
            }



            //foreach (var assembly in assemblies)
            //{
            //    AppDomain.CurrentDomain.Load()
            //}//var a2 = typeof(Uni.Plm.Test2.Module.Components.ModulePageComponent).GetTypeInfo().Assembly;
            //var embeddedFileProvider2 = new EmbeddedFileProvider(
            //    a2,
            //    "Uni.Plm.Test1.Module"
            //);
            var fileProviders =
                assemblies.Select(a => new EmbeddedFileProvider(a, a.GetName().Name));
            //Add the file provider to the Razor view engine
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.FileProviders.Add(new CompositeFileProvider(fileProviders));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "Bundles")),
                RequestPath = "/Bundles"
            });
            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "Bundles")),
                RequestPath = "/Bundles"
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areas",
                    template: "{area}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private IEnumerable<Assembly> GetModulesAssemblies(string pathToModules)
        {
            string absolutePath = Path.Combine(Environment.CurrentDirectory, pathToModules);

            var asmPaths = Directory.GetFiles(absolutePath)
                .Where(s => s.EndsWith(".dll", StringComparison.OrdinalIgnoreCase)).ToList();

           // var ams = asmPaths.Select(s => new AssemblyResolver(s).Assembly);
           /* var assemblyNames = asmPaths.Select(AssemblyLoadContext.GetAssemblyName);
            var loader = new AssemblyLoader(pathToModules);
            var asms = assemblyNames.Select(loader.LoadFromAssemblyName);*/

            return asmPaths.Select(s => Assembly.LoadFrom(s));
        }
    }
}

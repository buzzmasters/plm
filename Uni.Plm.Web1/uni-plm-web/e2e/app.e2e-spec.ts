import { UniPlmWebPage } from './app.po';

describe('uni-plm-web App', () => {
  let page: UniPlmWebPage;

  beforeEach(() => {
    page = new UniPlmWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
